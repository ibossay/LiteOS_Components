# 组件开发指南

## 组件定义

组件是`Huawei LiteOS`系统的组成部分，属于`Huawei LiteOS`应用。从系统层面看，除内核、构建脚本、辅助工具、系统函数库外，其余组成`Huawei LiteOS`系统功能的软件包均可称为组件。本文档将详细介绍组件的构成、规范及组件开发流程，帮助组件开发人员进行组件开发。

`Huawei LiteOS`的组件分为在线组件和离线组件，在线组件的组件源码不在`Huawei LiteOS`代码仓中，使用时需要从网上下载，而离线组件的源码则存放在`Huawei LiteOS`代码仓中。离线组件一般都是基础组件，如网络、文件系统等。


## 组件构成

下面以在线组件`curl`为例，对`Huawei LiteOS`的组件构成及组件管理进行详细说明。


### 目录结构

新增一个组件涉及新建及修改的文件或目录如下所示。其中components下的curl和demos下的curl及它们包含的文件为新增目录和文件，其余文件为各组件或demos的共有文件，新增组件时一般也需要修改。
```
.
└── Huawei_LiteOS
    ├── components
    │   ├── components.mk          // 调用第一层子目录中的.mk文件，并导出所有组件对外开放的头文件
    │   ├── download.sh            // 在线组件的下载脚本
    │   ├── Kconfig
    │   ├── Makefile
    │   ├── online_components      // 在线组件的下载信息
    │   └── utility
    │       ├── curl               // 组件目录
    │       │   ├── curl-7.54.1    // 组件源码
    │       │   ├── curl.mk        // 组件对外头文件的管理文件
    │       │   ├── Kconfig        // 组件配置文件
    │       │   ├── Makefile       // 组件编译文件
    │       │   ├── origin.patch   // 组件源码的补丁文件
    │       │   ├── patch.sha256   // 补丁文件的sha256校验码
    │       │   └── src.sha256     // 组件源码的sha256校验码
    │       ├── Kconfig            // 调用第一层子目录中的Kconfig
    │       ├── Makefile           // 调用第一层子目录中的Makefile
    │       └── utility.mk         // 调用第一层子目录中的.mk文件，并导出其中的组件对外开放的头文件
    ├── demos
    │   └── utility
    │       ├── curl
    │       │   ├── curl_demo.c
    │       │   ├── curl_demo.h
    │       │   ├── curl.mk
    │       │   ├── Kconfig
    │       │   ├── Makefile
    │       │   └── README_CN.md
    │       ├── Kconfig
    │       ├── Makefile
    │       └── utility.mk
    └── targets
        ├── bsp
        │   └── common
        │       └── demo_entry.c  // 各个demo的调用入口
        └── targets.mk            // 链接各组件及demo编译生成的库文件
```
**须知：** 新增组件或demo，都需要新建其自身的`Kconfig`、`Makefile`、`.mk`文件，本文档以新增组件为主题，新增demo的方法和新增组件类似，本文档不再赘述。


### online_components

在线组件的源码需要从网上下载，下载信息记录在`online_components`文件中。该文件具有特定格式，保存了各在线组件的相关信息（或称参数），如下所示，每个组件各有四个参数，各参数之间使用`&#&`分开。

`curl-7.54.1 &#& components/utility/curl &#& LOSCFG_COMPONENTS_CURL=y &#& https://github.com/curl/curl/archive/refs/tags/curl-7_54_1.zip`
* 参数一：组件源码名（一般命名为：`源码名-版本号`）。
* 参数二：组件所在路径（将组件源码下载到该目录下）。
* 参数三：组件使能的标志（用于判断是否下载组件源码，该标志就是组件`Kconfig`文件中的配置项）。
* 参数四：组件源码的下载地址或获取组件源码的命令（因后面涉及源码校验，推荐优先下载源码压缩包，校验起来更加方便快捷）。

**须知：** 在线组件目前支持如下几种下载方式：
* 下载压缩包：目前仅支持`.zip`/`.tar.gz`类型的压缩包，如：https://github.com/curl/curl/archive/refs/tags/curl-7_54_1.zip。
* 使用git克隆：如：`git clone -b curl-7.54.1 https://github.com/curl/curl.git`。
* 如无法使用以上方式获取源码，可自行编写shell脚本完成源码下载，并返回下载结果（成功/失败）。参数四填写调用该脚本的方法即可。


### download.sh

下载在线组件源码和补丁文件，进行sha256sum校验，校验成功后，将补丁文件打入源码。


### components/utility/curl

`curl`目录是curl组件的源码及其相关文件所在目录。新增组件时，请根据组件功能在`components`目录相应位置下新建组件目录（分为语言类组件`language`、媒体类组件`media`、工具类组件`utility`、安全组件`security`等。而curl属于工具类组件，故其位于`utility`目录下）。

命名规范：一般和组件源码同名（不可简写，如`libpng`不可写为`png`）或参考业界通用命名。


### components/utility/curl/curl-7.54.1

组件源码，目录名需要和`online_components`文件中的参数一保持一致。对于在线组件由`download.sh`从网上下载获得，所以不需要将组件源码上传到`Huawei LiteOS`代码仓，只有离线组件的源码需要上传。


### src.sha256

文件内容为组件源码的`sha256`校验码。为确保组件的正常使用，需要保证从网上下载的源码与开发组件时使用的源码是同一份，`LiteOS`通过对下载的源码进行`sha256`校验来保证是同一份源码。所以，开发组件时需要计算生成源文件的`sha256`校验码，并将其复制到`src.sha256`文件。`curl`组件的`src.sha256`的文件内容如下：

```
7eec2c7f863c17d8586e94255cd0c2934822f1ae3cb40283a892b46e44f93bdf  curl-7.54.1.zip
```

生成`sha256`校验码的方法可参考如下命令：

* 如果下载的源码包是压缩包：
  ```shell
  sha256sum 参数一.zip > src.sha256  # sha256sum curl-7.54.1.zip > src.sha256
  ```

* 如果源码包不是压缩包：
  ```shell
  find 参数一 -type f -print0 | xargs -0 sha256sum > src.sha256 # find curl-7.54.1 -type f -print0 | xargs -0 sha256sum > src.sha256
  ```

**须知：** 如果需要在`Windows`中执行以上命令，可以安装`Git`工具，使用`Git`自带的终端即可执行。注意需提前设置`Git`为禁止换行符转换，参考命令`git config --global core.autocrlf input`。

**注意：** 
- 校验码文件采用统一命名`src.sha256`，文件名不可修改。
- 开发组件时，请使用`online_components`文件中的参数四的方式下载源码文件。


### origin.patch

源码的补丁文件，该文件采用统一命名`origin.patch`，不可修改。在`Huawei LiteOS`上开发组件，建议尽量避免对开源源码的直接修改，如无法避免（例如：不修改源码无法编译成功，或源码无法直接运行在`Huawei LiteOS`上，则需要适配），则应该将修改内容生成为补丁文件`origin.patch`。在后续使用中，采取打补丁的方式修改源码。

补丁文件需上传到`LiteOS_Components`仓库中，使用时由`download.sh`从该仓库中下载获得。`download.sh`脚本会采用`patch -p1`的方式打补丁，所以应该将修改后的源码文件放在另一个源码目录中，对原源码目录和修改后的源码目录制作补丁。

**注意：** 请确保`origin.patch`文件在本地校验前已经是`unix`格式，因为该文件上传到`LiteOS_Components`仓库时会自动转换为`unix`格式，如果本地校验时不是`unix`格式，会导致该在线组件执行`download.sh`失败。

### patch.sha256

源码补丁文件`origin.patch`的校验码文件，该文件采用统一命名`patch.sha256`，不可修改。其作用是保证下载的补丁文件正确，文件格式与`src.patch`的要求一致。


### Kconfig

#### components/utility/curl/Kconfig

组件配置文件，通过该文件将组件纳入到LiteOS的菜单项界面中管理，编写时可参考已有组件的`Kconfig`文件进行修改，`Kconfig`的基本语法可自行搜索或参考官方文档<a href="https://www.kernel.org/doc/Documentation/kbuild/kconfig-language.txt?spm=a2c4g.11186623.2.2.65b57ecfdlKcCf&file=kconfig-language.txt" target="_blank">《kconfig-language.txt》</a>。

`components/utility/curl/Kconfig`的文件内容及说明如下：
```shell
config LOSCFG_COMPONENTS_CURL           # 配置项：一般固定为"LOSCFG_COMPONENTS_目录名"
    bool "Enable Curl"                  # bool: 配置项类型; Enable Curl: 配置项提示
    select LOSCFG_COMPONENTS_NET_LWIP   # 依赖关系：如依赖其他组件，请指明依赖关系
    select LOSCFG_COMPONENTS_FS_FATFS
    default n                           # 组件默认是否使能：一般默认不使能
    help
      Answer y to enable curl.          # 帮助信息：该配置项的说明
```
* 如果组件下还包含其他多个组件，可灵活命名配置项，如`LOSCFG_COMPONENTS_目录名_XXX`，但不宜过程。
* 配置项提示是menuconfig菜单项中的显示信息，可理解为配置项的别名，各单词首字母需大写，其余小写（专有名词除外)。
* 帮助信息的首字母大写，其余小写（专有名词除外），同时要有结束符`.`。
* 缩进请使用空格，不得使用`tab`健。

#### 上级目录中的Kconfig

新增组件时，除了需要在本级组件目录下新建本组件的`Kconfig`文件，还需要修改上级目录下的`Kconfig`文件。该文件用于调用下一级子目录中的Kconfig，可参考该文件中的其他组件进行修改。`components/utility/Kconfig`文件内容如下，其包含了`utility`目录下所有组件的`Kconfig`文件。
```shell
menu "Utility"                                      # 菜单项

source "components/utility/bidireference/Kconfig"
source "components/utility/curl/Kconfig"            # 本次新增的内容
source "components/utility/freetype/Kconfig"
source "components/utility/iconv/Kconfig"
source "components/utility/iniparser/Kconfig"
source "components/utility/json-c/Kconfig"
source "components/utility/libxml2/Kconfig"

endmenu
```


### .mk文件

`.mk`用于导出组件的对外头文件，供其他组件或模块调用本组件中的API。为避免头文件关系混乱，`Huawei LiteOS`使用`.mk`文件管理和限制组件对外头文件，并通过各目录下的`.mk`文件将组件的对外接口层层导出。最终所有组件的头文件通过`components.mk`对外提供。

新增组件时一般涉及的`.mk`文件为：
* 在新增组件目录下新建本组件的`.mk`文件，可参考已有组件的`.mk`文件。
* 修改上级目录下的`.mk`文件，可参考该文件中的其他组件进行修改。

如`curl.mk`与`utility.mk`：
```makefile
####################### curl.mk的内容如下 ########################
CURL_VERSION=curl-7.54.1   # 组件版本

COMPONENTS_CURL_INCLUDE := \
    -I $(LITEOSTOPDIR)/components/utility/curl/$(CURL_VERSION)/include \
    -I $(LITEOSTOPDIR)/components/utility/curl/$(CURL_VERSION)/src

##################### utility.mk的内容如下 #######################
ifeq ($(LOSCFG_COMPONENTS_CURL), y)                       # 只有组件已使能，才执行以下操作
include $(LITEOSTOPDIR)/components/utility/curl/curl.mk   # 调用子目录下其他组件的.mk文件
COMPONENTS_UTILITY_INCLUDE += $(COMPONENTS_CURL_INCLUDE)  # 导出该组件对外开放的头文件
endif
```
文件规范及说明：
* CURL_VERSION：组件版本宏定义，一般命名为`源码名_VERSION`。
* COMPONENTS_CURL_INCLUDE：头文件宏定义，一般命名为`COMPONENTS_源码名_INCLUDE`
* 一般在组件的上级目录`.mk`文件中，调用具体组件的`.mk`文件时必须有条件限制，只有组件被使能的情况下才允许调用组件的`.mk`文件。
* 如需缩进，统一缩进四个空格，注意不得使用tab健。


### Makefile

#### components/utility/curl/Makefile

组件化编译文件，通过该文件将组件加入到`Huawei LiteOS`的组件化编译框架中。新增组件时需要在组件目录下新建该文件以实现组件编译，可参考已有组件的`Makefile`文件。`components/utility/curl/Makefile`文件内容如下：

```makefile
include $(LITEOSTOPDIR)/config.mk
include $(LITEOSTOPDIR)/components/net/lwip/lwip.mk
include $(LITEOSTOPDIR)/components/utility/curl/curl.mk

MODULE_NAME := $(notdir $(CURDIR))      # $(notdir $(CURDIR)) 等价于 curl

LOCAL_SRCS_y :=
LOCAL_DIRS_y :=

LOCAL_DIRS_y += $(CURL_VERSION)/lib
LOCAL_DIRS_y += $(CURL_VERSION)/lib/vauth
LOCAL_DIRS_y += $(CURL_VERSION)/lib/vtls
LOCAL_DIRS_y += $(CURL_VERSION)/src

LOCAL_INCLUDE := $(LWIP_INCLUDE)
LOCAL_INCLUDE += \
    -I $(LITEOSTOPDIR)/components/utility/curl/$(CURL_VERSION)/lib \
    -I $(LITEOSTOPDIR)/components/utility/curl/$(CURL_VERSION)/include \
    -I $(LITEOSTOPDIR)/components/utility/curl/$(CURL_VERSION)/src

LOCAL_SRCS_y += $(foreach dir, $(LOCAL_DIRS_y), $(wildcard $(dir)/*.c))
LOCAL_SRCS   = $(LOCAL_SRCS_y)

LOCAL_EXT_FLAG := -Wno-error -Wno-implicit-function-declaration -Wno-unused-variable

CURL_DEFS = \
    -D HAVE_CONFIG_H \
    -D BUILDING_LIBCURL

LOCAL_FLAGS := $(LOCAL_INCLUDE) $(CURL_DEFS) $(LOCAL_EXT_FLAG)

include $(MODULE)
```
下面对几个重要参数进行说明（部分参数不是每个`Makefile`都必须的，请根据实际需求选择写/不写）：
* include $(LITEOSTOPDIR)/config.mk：引入`Huawei LiteOS`系统的宏定义，所有`Makefile`都需要引用该文件。
* MODULE_NAME：模块名，编译生成的静态链接库的名称就是模块名，例如这里模块名为`curl`，那么生成的静态链接库就是libcurl.a。在`targets.mk`文件中需要增加新组件的库文件的链接，请参考`targets.mk`文件中已有组件的链接进行修改。

  **注意：** 如果组件名本身是以`lib`开头的，模块名中不需要体现`lib`，如`libpng`应写为`MODULE_NAME := png`，而不应写为`MODULE_NAME := libpng`，避免生成的静态库文件名为`liblibpng.a`。
  
* LOCAL_INCLUDE：编译需要用到的头文件集合。
* LOCAL_SRCS：本模块需要参与编译的所有除头文件以外的源文件。
* LOCAL_EXT_FLAG：本模块的编译参数集合（非必须）。
* CURL_DEFS：本模块编译需要用到的宏定义集合（非必须）。
* 如需缩进，统一缩进四个空格，注意不得使用`tab`健。

#### 上级目录中的Makefile

该文件用于调用下一级子目录中的Makefile，`components/utility/Makefile`文件内容如下：

```makefile
include $(LITEOSTOPDIR)/config.mk

MODULE_$(LOSCFG_COMPONENTS_BIDIREFERENCE) += bidireference
MODULE_$(LOSCFG_COMPONENTS_CURL) += curl                    # 本次新增的内容
MODULE_$(LOSCFG_COMPONENTS_FREETYPE) += freetype
MODULE_$(LOSCFG_COMPONENTS_ICONV) += iconv
MODULE_$(LOSCFG_COMPONENTS_INIPARSER) += iniparser
MODULE_$(LOSCFG_COMPONENTS_JSON_C) += json-c
MODULE_$(LOSCFG_COMPONENTS_LIBXML2) += libxml2

include $(MODULE)
```
`LOSCFG_COMPONENTS_CURL`是组件`Kconfig`文件中的配置项，`MODULE_$(LOSCFG_COMPONENTS_CURL)`表示只有使能了组件后，才编译`curl`组件。


### 组件demo

原则上每个组件都应提供一个参考`demo`，demo必须自研，禁止直接拷贝网络上的代码。每个`demo`只允许提供一个对外接口，统一在`demo_entry.c`文件中进行调用。


### targets.mk

`Huawei LiteOS`采用组件化编译框架，每个组件或`demo`编译完成后会在`out`目录下生成相应的静态库文件。因`Windows`平台无法自动链接静态库文件，因此需要在`targets.mk`文件中进行手动链接。如新增链接`curl`组件和`demo`的静态库文件：
```makefile
ifneq ($(OS), Linux)
    ifeq ($(LOSCFG_COMPONENTS_CURL), y)
        LITEOS_BASELIB += -lcurl        # curl是组件的模块名
    endif

    ifeq ($(LOSCFG_DEMOS_CURL), y)
        LITEOS_BASELIB += -lcurl_demo   # curl_demo是组件demo的模块名
    endif
endif
```


### demo_entry.c

所有demo的入口，demo完成后，统一在该文件的`DemoEntry`函数中调用。

```c
#ifdef LOSCFG_DEMOS_CURL
#include "curl_demo.h"
#endif

VOID DemoEntry(VOID)
{
#ifdef LOSCFG_DEMOS_CURL
    CurlDemoTask();
#endif
}
```
**注意：** ，必须以`#include`头文件的方式调用函数，禁止使用`extern`的方式。


## 在线组件下载流程

### Linux平台

下载<a href="https://gitee.com/LiteOS/LiteOS" target="_blank">LiteOS代码仓</a>。在`Huawei LiteOS`源码根目录下执行`make menuconfig`命令后会自动调用`download.sh`脚本，该脚本读取`online_components`文件的内容以获取组件下载信息，判断组件是否被使能，如果组件已经使能，继续判断组件源码是否存在，如不存在则下载组件源码、`origin.patch`、`src.sha256`和`patch.sha256`。通过`.sha256`文件判断下载的组件源码及`origin.patch`文件是否正确，正确则打入patch，完成在线组件的下载流程。

**注意：** 如果想要重新下载组件源码和补丁，需要在本地删除已有源码和补丁。同时，在脚本运行过程中，如果被外部信号强制中断，如`ctrl z`、`ctrl c`等，会导致下载流程失败，请及时清理此次下载中残留文件。

### Windows平台

在`HUAWEI LiteOS Studio`开发工具中，完成`工程配置`——>`组件配置`后，会自动调用`download.sh`文件，其余逻辑和`Linux`平台一致。


## 测试组件

完成组件开发的全部工作后，需要对组件进行测试，确保[在线组件下载流程](#在线组件下载流程)执行通过。


## 提交代码

组件测试通过后，即可将代码提交到`Huawei LiteOS`的各代码仓，代码提交的要求如下：
1. 在线组件不需要提交组件源码。
2. `origin.patch`、`src.sha256`和`patch.sha256`提交到`LiteOS_Components`仓库下的对应目录（目录结构需与开发时的目录结构保持一致，例如`curl`的这些文件，应置于该仓库的`components/utility/curl`目录下）。对于离线组件，不需要`src.sha256`文件。
3. 其余文件提交到`LiteOS`仓库。

**须知：** 代码提交流程请参考<a href="https://gitee.com/LiteOS/LiteOS/blob/master/doc/LiteOS_Contribute_Guide.md" target="_blank">LiteOS 代码&文档贡献指南</a>。